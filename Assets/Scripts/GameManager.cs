﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    public GameObject restartMenu;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale=1;
        restartMenu.SetActive(false);
    }

    public void GameOver(){
        restartMenu.SetActive(true);
        //stoppo punteggio
        ScoreController.pointCounter=false;
        //stoppo il gioco
        Time.timeScale=0;
    }
   public void Replay(){
       SceneManager.LoadScene(0);
       restartMenu.SetActive(false);
   }
}
