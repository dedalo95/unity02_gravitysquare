﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreController : MonoBehaviour
{
    public static int score=0;
    private int counter = 0;
    public static bool pointCounter=true;
    private int highscore=0;
    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        pointCounter=true;
        highscore = PlayerPrefs.GetInt ("highscore", score);
    }

    // Update is called once per frame
    void Update()
    {
        //se il punteggio è un nuovo highscore
        if(score > highscore){
            //imposto nuovo highscore
            highscore = score;
            PlayerPrefs.SetInt ("highscore", highscore);
            PlayerPrefs.Save();
        }
        counter++;
        GetComponent<UnityEngine.UI.Text>().text = score.ToString();
        //se sono passati 25 frame => aumento punteggio
        if(counter % 30 == 0 && pointCounter==true){
            score++;
            counter=0;
        }
    }
}
