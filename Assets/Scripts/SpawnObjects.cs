﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour
{
    public float maxTime = 4;
	public float timer = 0;
    public GameObject enemy;
    public GameObject center;
    public GameObject doubleDown;
    public GameObject doubleUp;
    public GameObject doubleUpDown;
    public GameObject triple;
    public GameObject tripleUp;
    public GameObject upDown;
    public GameObject destroyer;

    // Start is called before the first frame update
    void Start()
    {
        GameObject newGO = Instantiate(enemy);
    }

    // Update is called once per frame
    void Update()
    {
        if(timer > maxTime){
            int minVal = 0;
            int maxVal = 8;
            int num = Random.Range(minVal, maxVal);
            //istanzio nuova pipe (casualmente)
            switch(num){
                case 0:
                    GameObject newEn = Instantiate(enemy);
                    Destroy(newEn,15);
                    break;
                case 1:
                    GameObject newCen = Instantiate(center);
                    Destroy(newCen,15);
                    break;
                case 2:
                    GameObject newDoubleDown = Instantiate(doubleDown);
                    Destroy(newDoubleDown,15);
                    break;
                case 3:
                    GameObject newDoubleUp = Instantiate(doubleUp);
                    Destroy(newDoubleUp,15);
                    break;
                case 4:
                    GameObject newDoubleUpDown = Instantiate(doubleUpDown);
                    Destroy(newDoubleUpDown,15);
                    break;
                case 5:
                    GameObject newTriple = Instantiate(triple);
                    Destroy(newTriple,15);
                    break;
                case 6:
                    GameObject newTripleUp = Instantiate(tripleUp);
                    Destroy(newTripleUp,15);
                    break;
                case 7:
                    GameObject newUpDown = Instantiate(upDown);
                    Destroy(newUpDown,15);
                    break;
                case 8:
                    GameObject newDestroyer = Instantiate(destroyer);
                    Destroy(newDestroyer,15);
                    break;
                default:
                    GameObject defaultGame = Instantiate(upDown);
                    Destroy(defaultGame,15);
                    break;
            }
			//distruggi dopo 15 secondi
			timer=0;
        }
        timer += Time.deltaTime;
    }
}
