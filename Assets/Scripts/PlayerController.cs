﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameManager gameManager;
    public GameObject particle;
    private bool closeGame = false;
    //contatore di frame
    private int counter = 75;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Se viene cliccata una qualsiasi parte dello schermo
        if (Input.GetMouseButtonDown(0)){
            //inverti gravità del personaggio
            rb.gravityScale *= -1;
        }
        //se è avviata la routine di chiusura gioco (attendo circa 3 secondi per chiudere il gioco)
        if(closeGame==true){
            //decremento counter
            counter--;
            //se è uguale a 0
            if(counter==0){
                //avvio game over
                gameManager.GameOver();
            }
        }
    }
    void OnCollisionEnter2D(Collision2D col){
        print(col.gameObject.tag);
        if(col.gameObject.tag=="Enemy" || col.gameObject.tag =="Moving"){
            gameObject.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            //genero tante particelle
            for (int i = 0; i < 50; i++){
                GameObject gb = Instantiate(particle, transform.position, Quaternion.identity);
            }
            //avvio routine chiusura partita
            closeGame = true;
        }
    }
}
